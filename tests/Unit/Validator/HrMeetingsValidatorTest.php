<?php

declare(strict_types=1);

namespace Tests\Unit\Validator;

use App\Exceptions\ApiException;
use App\Validator\HrMeetingsValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class HrMeetingsValidatorTest extends TestCase
{
    private HrMeetingsValidator $hrMeetingsValidator;
    private ValidatorInterface $validator;

    protected function setUp(): void
    {
        $this->hrMeetingsValidator = new HrMeetingsValidator();
        $this->validator = $this->getMockBuilder(ValidatorInterface::class)
            ->onlyMethods(['validate'])
            ->getMockForAbstractClass();

        $this->hrMeetingsValidator->setValidator($this->validator);
    }

    public function testHrMeetingServiceFail(): void
    {
        $this->expectException(ApiException::class);
        $this->expectExceptionCode(400);

        $constraintViolations = $this->getMockBuilder(ConstraintViolationListInterface::class)
            ->onlyMethods(['count'])
            ->getMockForAbstractClass();

        $constraintViolations
            ->expects($this->once())
            ->method('count')
            ->willReturn(1);

        $this->validator->method('validate')->willReturn($constraintViolations);

        $this->hrMeetingsValidator->validateDate('2021-01-011');
    }

    public function testHrMeetingServicePass(): void
    {
        $this->assertNull($this->hrMeetingsValidator->validateDate('2021-01-01'));
    }
}
