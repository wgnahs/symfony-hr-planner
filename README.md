# HR Meeting planner (demo)
I've created a HR meeting planner to fit the requirements of the recruitement process.

My stack:
- Symfony 6.1
- PHP 8.1
- REST API
- OpenAPI Specification
- Docker image for local development
- Gitlab CI/CD config to build the docker image after merging to the main branch (see /.gitlab-ci)
- A kubernetes config file (see /kubernetes-conf.yaml)
- A few PHP unit tests
- Usage of EventListeners
- Usage of Attributes according to the latest standards

# Demo requests
## GET Request
![Request](https://gitlab.com/wgnahs/symfony-hr-planner/-/raw/main/screenshots/api.png)

## POST request met validatie
![Request](https://gitlab.com/wgnahs/symfony-hr-planner/-/raw/main/screenshots/post.png)

## POST succesvol
![Request](https://gitlab.com/wgnahs/symfony-hr-planner/-/raw/main/screenshots/postsuccess.png)

## OpenAPI Specification
![OpenAPI](https://gitlab.com/wgnahs/symfony-hr-planner/-/raw/main/screenshots/openapi.png)

## Unit test
![Tests](https://gitlab.com/wgnahs/symfony-hr-planner/-/raw/main/screenshots/tests.png)
