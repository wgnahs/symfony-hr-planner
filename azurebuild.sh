#!/bin/sh
#Azurebuild.sh

RED='\033[0;31m'
GRN='\033[0;32m'
NC='\033[0m' # No Color
HR='=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-='

echo -e "Azure build script"
echo -e "${HR}"

echo "enter container name:"
read containername

echo "enter tag:"
read tag

echo "enter resource group:"
read resourcegroup

echo "enter dns label name:"
read DNS_NAME_LABEL

echo "enter registry username:"
read username

echo "enter registry password:"
read password

echo -e "${HR}"

echo -e "Start to create image for container $containername container"

docker build --platform linux/amd64 -t $containername .

docker login hrplanner.azurecr.io

docker tag $tag hrplanner.azurecr.io/$containername

docker push hrplanner.azurecr.io/$containername

echo -e "${GRN}Image created and pushed to Azure Container Registry${NC}"

echo -e "Creating Azure Container Instance"

az container create --resource-group $resourcegroup --name $containername --image hrplanner.azurecr.io/$containername --ports 80 --dns-name-label $DNS_NAME_LABEL --location eastus --registry-username $username --registry-password $password

echo -e "${GRN}Container created${NC}"

az container show --resource-group $resourcegroup --name $containername --query "{FQDN:ipAddress.fqdn,ProvisioningState:provisioningState}" --output table

echo -e "${GRN}Build is ready${NC}"

$SHELL

