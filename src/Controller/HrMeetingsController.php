<?php

namespace App\Controller;

use App\Service\ApiProcessingService;
use App\Service\HrMeetingsService;
use App\Validator\HrMeetingsValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HrMeetingsController extends AbstractController
{
    /**
     * List the upcoming HR meetings for given day
     */
    #[Route('/api/hr-meetings/{date}', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Returns the HR meetings for given day',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(
                type: 'object',
                example: [
                    'id' => new OA\Property(property: 'id', type: 'integer', example: 1),
                    'date' => new OA\Property(property: 'date', type: 'string', example: '2023-01-01'),
                    'time' => new OA\Property(property: 'time', type: 'string',  example: '10:00'),
                    'location' => new OA\Property(property: 'location', type: 'string', example: 'Amsterdam'),
                ]
            )
        )
    )]
    #[OA\Response(
        response: 400,
        description: 'Bad request',
        content: new OA\JsonContent(
            type: 'object',
            example: [
                'code' => new OA\Property(property: 'code', type: 'integer', example: 400),
                'message' => new OA\Property(property: 'message', type: 'string', example: 'Bad request'),
            ]
        )
    )]
    #[OA\Parameter(
        name: 'date',
        description: 'The field used to filter the HR meetings on date',
        in: 'path',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Tag(name: 'hr_meeting')]
    public function getUpcomingMeetings(string $date, HrMeetingsValidator $validator, HrMeetingsService $service): Response
    {
        // validate the date
        $validator->validateDate($date);

        // after validating, fetch the meetings
        $meetings = $service->getMeetingsByDate($date);

        // return the meetings
        return new JsonResponse([
            'response' => 'OK',
            'data' => $service->formatForApiResults($meetings)
        ]);
    }

    /**
     * Save a HR meeting
     */
    #[Route('/api/hr-meetings', methods: ['POST'])]
    #[OA\Post(
        description: 'Save a HR meeting',
        requestBody: new OA\RequestBody(
            description: 'The HR meeting to save',
            required: true,
            content: new OA\JsonContent(
                type: 'object',
                example: [
                    'date' => new OA\Property(property: 'date', type: 'string', example: '2023-01-01'),
                    'time' => new OA\Property(property: 'time', type: 'string',  example: '10:00:00'),
                    'location' => new OA\Property(property: 'location', type: 'string', example: 'Amsterdam'),
                ]
            )
        ),
        responses: [
            new OA\Response(
                response: 201,
                description: 'Returns the saved HR meeting',
                content: new OA\JsonContent(
                    type: 'object',
                    example: [
                        'id' => new OA\Property(property: 'id', type: 'integer', example: 1),
                        'date' => new OA\Property(property: 'date', type: 'string', example: '2023-01-01'),
                        'time' => new OA\Property(property: 'time', type: 'string',  example: '10:00:00'),
                        'location' => new OA\Property(property: 'location', type: 'string', example: 'Amsterdam'),
                    ]
                )
            ),
            new OA\Response(
                response: 400,
                description: 'Returns the validation errors',
                content: new OA\JsonContent(
                    type: 'object',
                    example: [
                        'response' => new OA\Property(property: 'response', type: 'string', example: 'ERROR'),
                        'errors' => new OA\Property(property: 'errors', type: 'array', example: [
                            'date' => new OA\Property(property: 'date', type: 'string', example: 'This value is not a valid date.'),
                            'time' => new OA\Property(property: 'time', type: 'string', example: 'This value is not a valid time.'),
                            'location' => new OA\Property(property: 'location', type: 'string', example: 'This value is not valid.'),
                        ])
                    ]
                )
            )
        ]
    )]
    #[OA\Tag(name: 'hr_meeting')]
    public function createNewMeeting(Request $request, HrMeetingsValidator $validator,
                                     HrMeetingsService $service, ApiProcessingService $apiProcessingService): Response
    {
        $data = $apiProcessingService->convertJsonToArray($request->getContent());

        // validate the input
        $validator->setInput($data);
        $validator->validatePostRequest();

        // Let's pretend that we saved the meeting using doctrine in the service :)
        // $meeting = $service->saveMeeting($data);

        // return the meetings
        return new JsonResponse([
            'response' => 'OK',
            'data' => $data
        ], 201);
    }
}