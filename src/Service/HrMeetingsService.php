<?php

namespace App\Service;

class HrMeetingsService
{
    /**
     * Hardcoded list of HR meetings for demo purposes
     */
    protected array $meetings = [
        [
            'id' => 1,
            'date' => '2021-01-01',
            'time' => '10:00:00',
            'location' => 'Amsterdam',
        ],
        [
            'id' => 2,
            'date' => '2021-01-02',
            'time' => '10:00:00',
            'location' => 'Amsterdam',
        ],
        [
            'id' => 3,
            'date' => '2021-01-03',
            'time' => '10:00:00',
            'location' => 'Amsterdam',
        ],
        [
            'id' => 4,
            'date' => '2021-01-04',
            'time' => '10:00:00',
            'location' => 'Amsterdam',
        ],
        [
            'id' => 5,
            'date' => '2021-01-05',
            'time' => '10:00:00',
            'location' => 'Amsterdam',
        ]
    ];


    /**
     * Fetch meetings by date
     *
     * @param string $date
     * @return array
     */
    public function getMeetingsByDate(string $date): array
    {
        return array_filter($this->meetings, function ($meeting) use ($date) {
            return $meeting['date'] === $date;
        });
    }

    /**
     * Fetch meetings by date and time
     *
     * @param string $date
     * @param string $time
     * @return array
     */
    public function getMeetingsByDateAndTime(string $date, string $time): array
    {
        return array_filter($this->meetings, function ($meeting) use ($date, $time) {
            return $meeting['date'] === $date && $meeting['time'] === $time;
        });
    }

    /**
     * Format the meetings for the API response
     *
     * @param array $meetings
     * @return array
     */
    public function formatForApiResults(array $meetings): array
    {
        return array_map(function ($meeting) {
            return [
                'id' => (int) $meeting['id'],
                'date' => $meeting['date'],
                'time' => date('H:i', strtotime($meeting['time'])),
                'location' => $meeting['location'],
            ];
        }, $meetings);
    }
}