<?php

namespace App\Service;

use App\Exceptions\ApiException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ApiProcessingService
{
    private ValidatorInterface $validator;

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @required
     * @param ValidatorInterface $validator
     */
    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    /**
     * Check if valid Json and then converts it to an array
     *
     * @param string $input
     * @return array
     */
    public function convertJsonToArray(string $input): array
    {
        $jsonConstraint = new Assert\Json();
        $errors = $this->validator->validate($input, $jsonConstraint);

        if ($errors->count() > 0) {
            throw new ApiException(400, 'general.invalidJson', 'Please enter a valid Json');
        }

        return json_decode($input, true);
    }
}