<?php

namespace App\EventListener;

use App\Exceptions\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionListener
{
    /**
     * Listens for exceptions and returns a JSON response
     *
     * @param ExceptionEvent $event
     * @return void
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        switch (true) {
            case ($exception instanceof ApiException):
                $result = ['result' => $exception->getExceptions()];
                $statusCode = $exception->getStatusCode();
                break;

            case ($exception instanceof NotFoundHttpException || $exception instanceof MethodNotAllowedHttpException):
                $result = [
                    'result' => [
                        [
                            'code' => 'general.routeNotFound',
                            'info' => 'The route was not found',
                        ],
                    ],
                ];
                $statusCode = 404;
                break;

            case ($exception instanceof \Exception):
            default:
                $result = [
                    'result' => [
                        [
                            'code' => 'general.generalError',
                            'info' => 'General error',
                        ],
                    ],
                ];

                $statusCode = 500;
        }

        $headers = [];

        $event->setResponse(new JsonResponse($result, $statusCode, $headers));
    }

    /**
     * Add a version header to the response after the controller is ready
     *
     * @param ResponseEvent $event
     */
    public function onKernelResponse(ResponseEvent $event)
    {
        $response = $event->getResponse();

        $response->headers->add([
            'x-api-version' => 'v1',
            'x-api-context' => 'test'
        ]);
    }
}