<?php

namespace App\Validator;

use App\Exceptions\ApiException;
use App\Service\HrMeetingsService;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class HrMeetingsValidator
{
    const LOCATIONS = ['Amsterdam', 'Arnhem'];

    #[Assert\Collection(
        fields: [
            'date' => [
                new Assert\NotBlank,
                new Assert\Date
            ],
            'time' => [
                new Assert\NotBlank,
                new Assert\Time
            ],
            'location' => [
                new Assert\NotBlank,
                new Assert\Length(
                    max: 100,
                    maxMessage: 'Location input is too long'
                ),
                new Assert\Choice(
                    choices: self::LOCATIONS,
                    message: 'Location input is not valid'
                )
            ]
        ],
        allowMissingFields: false,
    )]
    protected array $input;

    protected ValidatorInterface $validator;

    protected HrMeetingsService $hrMeetingsService;

    /**
     * @param array $input
     * @return void
     */
    public function setInput(array $input): void
    {
        $this->input = $input;
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @required
     * @param ValidatorInterface $validator
     */
    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    /**
     * @return HrMeetingsService
     */
    public function getHrMeetingsService(): HrMeetingsService
    {
        return $this->hrMeetingsService;
    }

    /**
     * @required
     * @param HrMeetingsService $hrMeetingsService
     */
    public function setHrMeetingsService(HrMeetingsService $hrMeetingsService): void
    {
        $this->hrMeetingsService = $hrMeetingsService;
    }

    /**
     * @param string $date
     * @return void
     */
    public function validateDate(string $date): void
    {
        $dateConstraint = new Assert\Date();
        $errors = $this->validator->validate($date, $dateConstraint);

        // in invalid, throw an exception
        if ($errors->count()) {
            throw new ApiException(400, 'general.invalidDate', 'Please enter a valid date yyyy-mm-dd');
        }
    }

    /**
     * @return void
     * @throws ApiException
     */
    public function validatePostRequest(): void
    {
        $errors = $this->validator->validateProperty($this, 'input');

        if ($errors->count() > 0) {
            foreach ($errors as $error) {
                throw new ApiException(400, 'general.invalidInput', $error->getMessage());
            }
        }

        // get the current records to make sure the date is not already taken, because we can only plan one meeting per day
        $dateTaken = $this->hrMeetingsService->getMeetingsByDateAndTime($this->input['date'], $this->input['time']);

        if ($dateTaken) {
            throw new ApiException(422, 'general.invalidInput', 'Date is already taken');
        }
    }
}