<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class HrMeeting {
    private int $id;

    #[Assert\NotBlank]
    public \DateTime $date;

    #[Assert\NotBlank]
    public \DateTime $time;

    #[Assert\NotBlank]
    public string $location;
}