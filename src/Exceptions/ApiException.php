<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiException extends HttpException
{
    private array $exceptions = [];

    /**
     * ApiException constructor.
     *
     * @param int $code
     * @param string $messageCode
     * @param string $info
     * @param \Exception|null $previous
     */
    public function __construct(int $code = 0, string $messageCode = '', string $info = '', \Exception $previous = null)
    {
        if ($messageCode || $info) {
            $this->code = $code;
            $this->addException($messageCode, $info);
        }

        parent::__construct($code, $info, $previous);
    }

    /**
     * Adds exception to the array of exceptions
     *
     * @param string $messageCode
     * @param string|null $info
     */
    public function addException(string $messageCode, string $info = null)
    {
        $this->exceptions[] = [
            'code' => $messageCode,
            'info' => $info,
        ];
    }

    /**
     * @return array
     */
    public function getExceptions(): array
    {
        return $this->exceptions;
    }

    /**
     * @return bool
     */
    public function hasException(): bool
    {
        return (bool)count($this->exceptions);
    }
}